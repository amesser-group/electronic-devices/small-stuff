EESchema Schematic File Version 4
LIBS:soic8-dip8-adaptor-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SOIC-8 to DIP-8W adaptor"
Date "2020-12-31"
Rev "1"
Comp "Copyright (c) 2020 Andreas Messer"
Comment1 "This work is licensed under a Creative Commons Attribution 4.0 International License."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:MCP6002-xSN U1
U 1 1 5FEDCDDB
P 1200 1050
F 0 "U1" H 1200 1250 50  0000 C CNN
F 1 "MCP6002-xSN" H 1200 1350 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 1200 1050 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21733j.pdf" H 1200 1050 50  0001 C CNN
	1    1200 1050
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:MCP6002-xSN U1
U 2 1 5FEDCE49
P 2400 1150
F 0 "U1" H 2400 1450 50  0000 C CNN
F 1 "MCP6002-xSN" H 2400 1426 50  0001 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 2400 1150 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21733j.pdf" H 2400 1150 50  0001 C CNN
	2    2400 1150
	-1   0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:MCP6002-xSN U1
U 3 1 5FEDCE69
P 850 1050
F 0 "U1" H 550 1050 50  0000 L CNN
F 1 "MCP6002-xSN" H 808 1005 50  0001 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 850 1050 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21733j.pdf" H 850 1050 50  0001 C CNN
	3    850  1050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Counter_Clockwise J1
U 1 1 5FEDD016
P 1750 1150
F 0 "J1" H 1800 1467 50  0000 C CNN
F 1 "DIL8" H 1800 1376 50  0000 C CNN
F 2 "Housings_DIP:DIP-8_W10.16mm" H 1750 1150 50  0001 C CNN
F 3 "~" H 1750 1150 50  0001 C CNN
	1    1750 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1050 1550 1050
Wire Wire Line
	900  1150 850  1150
Wire Wire Line
	850  1150 850  1300
Wire Wire Line
	850  1300 1400 1300
Wire Wire Line
	1400 1300 1400 1150
Wire Wire Line
	1400 1150 1550 1150
Wire Wire Line
	1550 1250 1450 1250
Wire Wire Line
	1450 1250 1450 1350
Wire Wire Line
	1450 1350 800  1350
Wire Wire Line
	800  1350 800  950 
Wire Wire Line
	800  950  900  950 
Wire Wire Line
	2050 1150 2100 1150
Wire Wire Line
	2050 1250 2200 1250
Wire Wire Line
	2200 1250 2200 1400
Wire Wire Line
	2200 1400 2750 1400
Wire Wire Line
	2750 1400 2750 1250
Wire Wire Line
	2750 1250 2700 1250
Wire Wire Line
	2050 1350 2150 1350
Wire Wire Line
	2150 1350 2150 1450
Wire Wire Line
	2150 1450 2800 1450
Wire Wire Line
	2800 1450 2800 1050
Wire Wire Line
	2800 1050 2700 1050
Wire Wire Line
	1550 1350 1500 1350
Wire Wire Line
	1500 1400 750  1400
Wire Wire Line
	750  1400 750  1350
Wire Wire Line
	1500 1350 1500 1400
Wire Wire Line
	750  750  750  700 
Wire Wire Line
	750  700  2100 700 
Wire Wire Line
	2100 700  2100 1050
Wire Wire Line
	2100 1050 2050 1050
$EndSCHEMATC
