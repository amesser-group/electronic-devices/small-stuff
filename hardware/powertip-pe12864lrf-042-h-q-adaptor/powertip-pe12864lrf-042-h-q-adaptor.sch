EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Powertip PE12864LRF-042-H-Q Breadboard adapter"
Date "2020-12-30"
Rev "1"
Comp "Copyright (c) 2020 Andreas Messer"
Comment1 "This work is licensed under a Creative Commons Attribution 4.0 International License."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x18_Female J2
U 1 1 5FECA72B
P 2350 1750
F 0 "J2" H 2377 1726 50  0000 L CNN
F 1 "18Pin FPC Top Contact" H 2377 1635 50  0000 L CNN
F 2 "Connector_FFC-FPC:TE_1-84953-8_1x18-1MP_P1.0mm_Horizontal" H 2350 1750 50  0001 C CNN
F 3 "~" H 2350 1750 50  0001 C CNN
	1    2350 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5FECD275
P 1850 950
F 0 "C1" V 2000 950 50  0000 C CNN
F 1 "22n" V 1700 950 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1888 800 50  0001 C CNN
F 3 "~" H 1850 950 50  0001 C CNN
	1    1850 950 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1850 2350 2150 2350
$Comp
L power:GND #PWR02
U 1 1 5FED227E
P 2050 1100
F 0 "#PWR02" H 2050 850 50  0001 C CNN
F 1 "GND" H 2055 927 50  0001 C CNN
F 2 "" H 2050 1100 50  0001 C CNN
F 3 "" H 2050 1100 50  0001 C CNN
	1    2050 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 950  2050 950 
Wire Wire Line
	2050 950  2000 950 
Wire Wire Line
	2050 1100 2050 950 
Connection ~ 2050 950 
NoConn ~ 2150 1150
Wire Wire Line
	2100 2250 2150 2250
Wire Wire Line
	2100 2150 2150 2150
Wire Wire Line
	2100 2050 2150 2050
Wire Wire Line
	2100 1950 2150 1950
Wire Wire Line
	2100 1850 2150 1850
Wire Wire Line
	2150 1750 2100 1750
Wire Wire Line
	2150 1650 2100 1650
Wire Wire Line
	2100 2150 2100 2250
Connection ~ 2100 2150
Wire Wire Line
	2100 2050 2100 2150
Connection ~ 2100 2050
Wire Wire Line
	2100 1950 2100 2050
Connection ~ 2100 1950
Wire Wire Line
	2100 1850 2100 1950
Connection ~ 2100 1850
Wire Wire Line
	2100 1750 2100 1850
Connection ~ 2100 1750
Wire Wire Line
	2100 1650 2100 1750
Connection ~ 2100 1650
Wire Wire Line
	2100 1550 1650 1550
Wire Wire Line
	2150 1550 2100 1550
Connection ~ 2100 1550
Wire Wire Line
	2100 1550 2100 1650
Wire Wire Line
	1650 1550 1650 1050
Wire Wire Line
	2100 2250 2100 2550
Wire Wire Line
	2100 2550 2150 2550
Connection ~ 2100 2250
$Comp
L Connector:Conn_01x08_Male J1
U 1 1 5FEEBB8E
P 1150 1450
F 0 "J1" H 1256 1928 50  0000 C CNN
F 1 "Pinheader" H 1256 1837 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 1150 1450 50  0001 C CNN
F 3 "~" H 1150 1450 50  0001 C CNN
	1    1150 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1550 1350 1550
Connection ~ 1650 1550
Wire Wire Line
	1350 1650 1850 1650
Wire Wire Line
	1850 1650 1850 2350
Wire Wire Line
	2150 2450 1750 2450
Wire Wire Line
	1750 2450 1750 1750
Wire Wire Line
	1750 1750 1350 1750
Wire Wire Line
	1350 1850 1650 1850
Wire Wire Line
	1650 1850 1650 2650
Wire Wire Line
	1650 2650 2150 2650
$Comp
L power:GND #PWR01
U 1 1 5FEF008E
P 1500 1150
F 0 "#PWR01" H 1500 900 50  0001 C CNN
F 1 "GND" H 1505 977 50  0001 C CNN
F 2 "" H 1500 1150 50  0001 C CNN
F 3 "" H 1500 1150 50  0001 C CNN
	1    1500 1150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 1150 1500 1150
Text Label 1350 1250 0    50   ~ 0
~RES~
Text Label 1350 1350 0    50   ~ 0
~CS~
Text Label 1350 1450 0    50   ~ 0
RS
Text Label 1350 1550 0    50   ~ 0
VDD
Text Label 1350 1650 0    50   ~ 0
SCK
Text Label 1350 1750 0    50   ~ 0
SI
Text Label 1350 1850 0    50   ~ 0
LED_K
Wire Wire Line
	1350 1450 2150 1450
Wire Wire Line
	1350 1350 2150 1350
Wire Wire Line
	1350 1250 2150 1250
Wire Wire Line
	1700 950  1650 950 
Wire Wire Line
	1650 950  1650 1050
Connection ~ 1650 1050
Wire Wire Line
	1650 1050 2150 1050
$EndSCHEMATC
