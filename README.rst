############################
Small helper circuits & PCBs
############################

A collection of some helper circuits / development aids.

License
=======

Copyright (C) 2020 Andreas Messer <andi@bastelmap.de>

All circuits are published under the terms of 
Creative Commons Attribution 4.0 International License (CC BY 4.0).
See https://creativecommons.org/licenses/by/4.0/legalcode for 
more details.


